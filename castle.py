''' Castle Adventure

Python Remake of old (DOS game)[https://en.wikipedia.org/wiki/Castle_Adventure]

Cross-platform, no dependencies. Requires terminal that supports ANSI
escape codes, including the terminal in Windows 10.

Classic mode:

    python castle.py

Also suppors an emoji mode where the objects and characters are emojis!
Requires a supported font in the terminal.

    python castle.py -e


A suitable font may need to be installed, for example:

    sudo apt install fonts-emojione


Known differences from the original:
    - No save/load functionality
    - No high scores
    - No sound
    - No intro screen or instructions

Tried to maintain the spelling mistakes throughout, that's
half the fun. Not having the original source, there's probably
commands and actions that are not implemented.
'''

import sys
import time
import shutil
import textwrap

try:
    from ctypes import windll
except ImportError:
    pass  # Not on windows, ANSI codes should work automatically
else:
    # On windows10, need to enable ANSI codes
    kernel = windll.kernel32
    kernel.SetConsoleMode(kernel.GetStdHandle(-11), 7)


# Cross-platform get-character functions
#---------------------------------------

class _Getch:
    ''' Gets a single character from standard input  Does not
        echo to the screen.
    '''
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self, timeout=None):
        char = self.impl(timeout=timeout)
        key = self.impl.decode(char)
        return key


class _GetchUnix:
    def __init__(self):
        import tty

    def __call__(self, timeout=None):
        import tty
        import termios
        from select import select

        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)

        ch = None
        try:
            tty.setraw(sys.stdin.fileno())
            i, o, e = select([sys.stdin.fileno()], [], [], timeout)
            if i:
                ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    def decode(self, char):
        ''' Decode key into human-readable character or key name '''
        if char is None:
            return None
        elif char == '\x03':
            raise KeyboardInterrupt
        elif char == '\x04':
            raise EOFError
        elif ord(char) == 127:
            key = 'backspace'
        elif char in ['\n', '\r']:
            key = 'enter'
        elif char == '\t':
            key = 'tab'
        elif char == '\x1b':
            # Escape char, need two more bytes
            char += sys.stdin.read(2)
            key = {'[A': 'up',
                   '[B': 'down',
                   '[C': 'right',
                   '[D': 'left',
                   '[F': 'end',
                   '[H': 'home',
                   '[3': 'delete',
                   '[2': 'insert',
                   '[5': 'pgup',
                   '[6': 'pgdn',
                   }.get(char[1:], None)
        else:
            key = str(char)
        return key


class _GetchWindows:
    ''' Windows-compatible get-character '''
    def __init__(self):
        import msvcrt

    def __call__(self, timeout=None):
        import msvcrt
        starttime = time.time()
        while True:
            if msvcrt.kbhit():
                return msvcrt.getch()

            if timeout is not None and time.time() - starttime > timeout:
                return None

    def decode(self, char):
        ''' Decode key into human-readable character or key name '''
        if char is None:
            return None
        elif char == b'\x03':
            raise KeyboardInterrupt
        elif char == b'\x04':
            raise EOFError
        elif char == b'\x08':
            key = 'backspace'
        elif char in [b'\n', b'\r']:
            key = 'enter'
        elif char == b'\t':
            key = 'tab'
        elif char in [b'\xe0', b'\x00']:  # 00 for numpad, e0 for arrows
            # Escape char, need two more bytes
            char2 = self()
            key = {b'H': 'up',
                   b'P': 'down',
                   b'M': 'right',
                   b'K': 'left',
                   b'G': 'upleft',
                   b'I': 'upright',
                   b'O': 'downleft',
                   b'Q': 'downright',
                   b'S': 'delete',
                   }.get(char2, char)
        else:
            key = char.decode()
        return key

getch = _Getch()  # Select appropriate getch function for OS


# Screen/printing commands
#---------------------------

def clear_screen():
    ''' Clear the entire screen '''
    print('\x1b[2J\x1b[H', end='', flush=True)

def hide_cursor():
    ''' Hide the terminal cursor '''
    print('\x1b[?25l')

def printxy(y, x, s):
    ''' Print string at x, y position '''
    print(f'\x1b[{y};{x}H{s}', end='', flush=True)


    
# Character definitions
#----------------------


emoji = '-e' in sys.argv[1:]   # Enable emoji mode
CHAR = {'player': '\U00002663',
        'wall':   '\U00002592',
        'gate':   '\U00002550',
        'hline':  '\U00002500',
        'vline':  '\U00002502',
        'boxLL':  '\U00002514',
        'boxLR':  '\U00002518',
        'boxUL':  '\U0000250C',
        'boxUR':  '\U00002510',
        'teeR':   '\U0000251C',
        'solid':  '\U00002588',
        'wall2':  '\U00002591',
        'wall3':  '\U00002593',
        'fount':  '\U00000393',
        'water':  '\U00002591',
        'shrub1': '\U00002591',
        'shrub2': '\U00002593',
        'statue': '\U000003B1',
        'lamp':   '\U00002660',
        'sword':  '\U0000253C',
        'demon':  '\U0000263B',
        'ogre':   '\U0000263A',
        'snake':  '\U000003B4',
        'crown':  '\U00002302',
        'flask':  '\U00000021',
        'bars':   '\U00002261',
        'goblet': '\U000000B5',
        'hourglass': '\U000003A6',
        'helmet':  '\U000000A2',
        'wand':    '\U00002500',
        'vampire': '\U00002663',
        'figurine': '\U000000A5',
        'fairy':   '\U000000F7',
        'diamond': '\U00002666',
        'gem': '\U00000398',
        'necklace': '\U000000A7',
        'key': '\U000003C4',
        'harp': '\U0000266B',
        'book': '\U000025AC',
        'glasses': '\U0000221E',
        'cross': '\U00000074',
        'ruby': '\U0000003A',
        'ball': '\U000000B0',
        'door': '\U000025D8',
        'bat': '\U00000028',
        'sspider': '\U0000002A',
        'bspider': '\U0000263C',
        'gold': '\U000025A0',
        'scepter': '\U000000DF',
        'chain': '\U00002248',
        'up': 'U',
        'dn': 'D',
        }

widthmultiple = 1  # Characters are 1 column wide

if emoji:
    widthmultiple = 2  # Characters are 2 columns wide
    CHAR.update({'player': '\N{slightly smiling face}',
                 'up': '\N{up-pointing small red triangle}',
                 'dn': '\N{down-pointing small red triangle}',
                 'wall': CHAR['wall'] * 2, #'\N{european castle}',
                 'gate': '\N{construction sign}',
                 'hline': CHAR['hline'] * 2,
                 'vline': CHAR['vline'] * 2,
                 'solid': CHAR['solid'] * 2,
                 'boxLL':  '\U00002514' + CHAR['hline'],
                 'boxLR':  CHAR['hline'] + '\U00002518',
                 'boxUL':  '\U0000250C' + CHAR['hline'],
                 'boxUR':  CHAR['hline'] + '\U00002510',
                 'teeR':   '\U0000251C',
                 'fount': '\N{fountain}',
                 'water': '\N{water wave}',
                 'shrub1': '\N{deciduous tree}',
                 'shrub2': '\N{palm tree}',
                 'statue': '\N{statue of liberty}',
                 'lamp': '\N{electric light bulb}',
                 'sword': '\N{dagger knife}',
                 'demon': '\N{japanese goblin}',
                 'ogre': '\N{japanese ogre}',
                 'snake': '\N{snake}',
                 'crown': '\N{crown}',
                 'flask': '\N{sake bottle and cup}',
                 'bars': '\N{spoon}',  # Silver bars, sliver spoon...  same thing.
                 'goblet': '\N{trophy}',
                 'hourglass': '\N{hourglass}',
                 'helmet': '\N{helmet with white cross}',
                 'wand': '\N{sparkles}', #'\N{magic wand}',  # Coming in unicode v13
                 'vampire': '\N{bat}',   #'\N{vampire}',  # Don't know why vampire icon just doesn't show up
                 'figurine': '\N{moyai}',
                 'fairy': '\N{princess}',  #'\N{fairy}',  # Fairy icon doesn't show up either
                 'diamond': '\N{gem stone}',
                 'gem': '\N{ring}',
                 'necklace': '\N{prayer beads}',
                 'key': '\N{key}',
                 'harp': '\N{violin}',  # A violin is a harp, right??
                 'book': '\N{book}',
                 'glasses': '\N{eyeglasses}',
                 'cross': '\N{latin cross}',
                 'ruby': '\N{up-pointing red triangle}',
                 'ball': '\N{crystal ball}',
                 'door': '\N{door}',
                 'bat': '\N{bat}',
                 'sspider': '\N{spider}',
                 'bspider': '\N{spider}',
                 'gold': '\N{sports medal}',
                 'scepter': '\N{hot dog}',  # Uh...
                 'chain': '\N{link symbol}',
                })
UPDN = [CHAR['up'], CHAR['dn']]
    

# Generic game classes
#---------------------------

class Tile(object):
    ''' Non-movable object (wall, door, staircase, etc) '''
    def __init__(self, char='X', color='white', collide=None):
        self.char = char
        self.color = color
        self.collidemsg = collide
        if collide is None and char in UPDN:
            self.collidemsg = char
        
    def collide(self):
        ''' Player ran into this tile '''
        # move to new room/floor, show message, or do nothing
        return self.collidemsg


class Room(object):
    ''' Defines a room '''
    def __init__(self, desc='', name=None):
        self.width = 24
        self.height = 18
        self.map = [[None for i in range(self.height)] for j in range(self.width)]
        self.desc = desc
        self.exits = {}  # Custom room exits for when map doesn't line up
        self.actions = {}
        self.name = name  # For checking if we're in the right room later
        
    def interact(self, verb, obj, xy=None, objects=None):
        ''' Interact with the room '''
        monsters = [o.name for o in objects.values() if isinstance(o, Monster)]
        if obj in monsters:
            msg = objects[obj].interact(verb)
        else:
            action = self.actions.get((verb, obj), None)
            if callable(action):
                msg = action()
            else:
                msg = action
        return msg
        
    def draw(self, scr):
        ''' Draw the room '''
        scr.clear()
        for x in range(self.width):
            for y in range(self.height):
                if self.map[x][y] is not None:
                    scr.addstr(y, x*widthmultiple, self.map[x][y].char)

    def drawdesc(self, scr):
        ''' Draw description of the room '''
        scr.clear()
        scr.addstr(0, 0, textwrap.fill(self.desc, self.width))
                    
    def drawitems(self, scr):
        ''' Draw list of items in the room '''
        scr.clear()
        itemcnt = 0
        for x in range(self.width):
            for y in range(self.height):
                if isinstance(self.map[x][y], Object) and itemcnt < 16:
                    scr.addstr(itemcnt % 8, 0+15*(itemcnt//8), str(self.map[x][y]))
                    itemcnt += 1

    def makeline(self, x, y, delta, vert=True, char=CHAR['wall'], color='white', collide=None):
        ''' Make wall as a line '''
        step = -1 if delta < 0 else 1
        if vert:
            for i in range(0, delta, step):
                self.map[x][y+i] = Tile(char, color, collide=collide)
        else:
            for i in range(0, delta, step):
                self.map[x+i][y] = Tile(char, color, collide=collide)

    def makewalls(self, coords, char=CHAR['wall'], color='white', collide=None):
        ''' Define walls of the room '''
        for i in range(len(coords)-1):
            if coords[i][0] == coords[i+1][0]:  # x-coordinate is the same
                vert = True
                delta = coords[i+1][1] - coords[i][1]
            else:                               # y-coordinate is the same
                vert = False
                delta = coords[i+1][0] - coords[i][0]
            delta = delta + 1 if delta > 0 else delta - 1
            self.makeline(coords[i][0], coords[i][1], delta=delta, vert=vert, char=char, color=color, collide=collide)


class Object(object):
    ''' Interactive objects '''
    def __init__(self, x, y, location, char, name, desc, adjective='', score=True, color='white'):
        self.x = x
        self.y = y
        self.loc = location  # Could be room or inventory
        self.char = char
        self.color = color
        self.name = name      # Name used for actions   (e.g. GOBLET)
        self.adj = adjective  # Adj. used in description (e.g. FANCY)
        self.desc = desc
        self.actions = {}
        self.score = score    # Whether this object is worth points at the end

        if isinstance(self.loc, Room):
            self.loc.map[x][y] = self
        
    def __str__(self):
        return self.char + ' {}'.format((' '.join([self.adj, self.name])).strip())
        
    def move(self, x, y):
        ''' Move the object to new location '''
        if isinstance(self.loc, Room):
            self.loc.map[self.x][self.y] = None
            self.loc.map[x][y] = self
        self.x, self.y = x, y
            
    def collide(self):
        ''' Collide with object, move to inventory if not full '''
        return self
    
    def interact(self, verb, room=None, xy=None):
        ''' Respond to interactions. Sublcass to customize, but call
            super to get default responses.
        '''
        if self.loc == 'inv':
            if verb in self.actions:
                msg = self.actions[verb]()
            elif verb == 'DROP':
                if room.map[xy[0]][xy[1]+1] is None:
                    self.loc = room
                    self.loc.map[xy[0]][xy[1]+1] = self
                    self.x, self.y = xy[0], xy[1]+1
                    msg = 'Done.'
                else:
                    msg = 'Something is in the way!'

            elif verb == 'SHOW':
                if self.currentroom.name == 'vampire':
                    msg = 'The VAMPIRE looks at you Funny!'
                elif self.currentroom.name == 'fairy':
                    msg = 'The FAIRY looks at you Funny!'
                elif self.monster:
                    msg = 'The {} looks at you Funny!'.format(self.monster.name)
                else:
                    msg = "There's not anyone to show it to!"

            else:
                msg = {'LOOK': self.desc,
                       'RUB': 'The {} is cleaner now.'.format(self.name),
                       'WAVE': 'You look awful Silly waving that {}'.format(self.name),
                       'DRINK': "You don't have any {}!".format(self.name),  # Yes, that's what it says even in your inv
                       'OPEN': "You don't have a key!",
                       'GET': 'You already have it!',
                       'EAT': "I'm not hungry!",
                       'WEAR': 'That could be Difficult!',
                       'PLAY': 'The {} makes Horrible music!!'.format(self.name),
                        }.get(verb, 'What?')

        elif self.loc == room:
            # In the room but haven't picked up yet
            msg = {'GET': 'Get it yourself!'}.get(verb, "You don't have it!")

        else:
            msg = {'GET': "I don't see a {}!".format(self.name)}.get(verb, "You don't have a {}!".format(self.name))

        return msg


class Monster(Object):
    ''' Monsters move toward player '''
    def __init__(self, x, y, location, char, name, desc, adjective='', color='white'):
        super(Monster, self).__init__(x, y, location, char, name, desc, adjective=adjective, color=color)
        self.health = 10
        self.desc = 'The {} looks mean!'.format(name)

    def die(self):
        ''' Monster is dead '''
        self.desc = 'The {} looks dead!'.format(self.name)
        self.adj = 'DEAD'

    def interact(self, verb, room=None, xy=None):
        ''' Respond to interactions. Sublcass to customize, but call
            super to get default responses.
        '''
        if verb == 'LOOK':
            return self.desc
        return 'What?'


class Trap(object):
    ''' Traps fill with water(?) and end the game '''
    def __init__(self, walls, fill):
        self.walls = walls
        self.fill = fill
        self.char = ' '

    def collide(self):
        ''' Ran in to trap '''
        return self    


class Game(object):
    ''' Game state '''
    def __init__(self, mapdim=(9, 6, 6)):  # (z, y, x)
        self.rooms = [[[None for i in range(mapdim[0])] for j in range(mapdim[1])] for k in range(mapdim[2])]
        self.objects = {}
        self.makerooms()

        self.roomx, self.roomy, self.roomz = 1, 0, 1
        self.currentroom = self.rooms[self.roomx][self.roomy][self.roomz]
        self.health = 15
        self.x, self.y = 10, 10
        self.monster = None
        self.cmdbuf = ''  # keyboard command buffer

    def set_windows(self, room, desc, objs, msg, cmd, box):
        ''' Store window areas to draw things in '''
        self.win = {}
        self.win['room'] = room
        self.win['desc'] = desc
        self.win['objs'] = objs
        self.win['msg'] = msg
        self.win['cmd'] = cmd
        self.win['box'] = box

    def checkinv(self):
        ''' Check inventory. Return True if there's room for more '''
        MAXINV = 6
        inv = len([i for i in self.objects.values() if i.loc == 'inv'])
        if inv >= MAXINV:
            return False
        return True

    def getroom(self, name):
        ''' Get room by name '''
        if name == 'current':
            return self.currentroom

        def flatten(list_to_flatten):
            for elem in list_to_flatten:
                if isinstance(elem, (list, tuple)):
                    for x in flatten(elem):
                        yield x
                else:
                    yield elem
        rooms = list(flatten(self.rooms))
        try:
            idx = [r.name if r is not None else None for r in rooms].index(name)
        except IndexError:
            return None
        else:
            return rooms[idx]

    def move(self, dir):
        ''' Move player and process actions '''
        if dir == 'N':
            newx, newy = self.x, self.y-1
        elif dir == 'S':
            newx, newy = self.x, self.y+1
        elif dir == 'E':
            newx, newy = self.x+1, self.y
        else:
            newx, newy = self.x-1, self.y

        # Check for new room
        if newx < 0 or newx >= roomw//widthmultiple or newy < 0 or newy >= roomh:
            if dir == 'N':
                self.y = roomh-1
                self.roomx, self.roomy, self.roomz = self.currentroom.exits.get('N', (self.roomx, self.roomy+1, self.roomz))
            elif dir == 'S':
                self.y = 0
                self.roomx, self.roomy, self.roomz = self.currentroom.exits.get('S', (self.roomx, self.roomy-1, self.roomz))
            elif dir == 'W':
                self.x = roomw//widthmultiple-1
                self.roomx, self.roomy, self.roomz = self.currentroom.exits.get('W', (self.roomx-1, self.roomy, self.roomz))
            else:
                self.x = 0
                self.roomx, self.roomy, self.roomz = self.currentroom.exits.get('E', (self.roomx+1, self.roomy, self.roomz))

            if self.roomy == -1:
                self.currentroom = None
                return False

            self.currentroom = self.rooms[self.roomx][self.roomy][self.roomz]
            self.monster = [obj for obj in self.objects.values() if isinstance(obj, Monster) and obj.loc == self.currentroom]
            self.monster = self.monster[0] if len(self.monster) > 0 else None
            self.drawroom()
            self.drawitems()
            self.drawplayer()
            self.drawmsg('')

        elif self.currentroom.map[newx][newy] is None:
            # Move player if nothing is there
            self.clearplayer()
            self.x, self.y = newx, newy
            self.drawplayer()

        else:
            # Collided with something
            msg = self.currentroom.map[newx][newy].collide()
            if isinstance(msg, Monster):
                monster = msg
                if self.objects['SWORD'].loc == 'inv' and monster.health > 0:
                    monster.health -= 1
                    if monster.health <= 0:
                        self.drawmsg('You killed the {}!'.format(monster.name))
                        monster.die()
                    else:
                        self.drawmsg('You struck the {}!'.format(msg.name))
                elif monster.health > 0:
                    self.drawmsg('You have no weapon!')

            elif isinstance(msg, Trap):
                # Hit a trap.
                if not self.objects['NECKLACE'].loc == 'inv':
                    self.currentroom.makewalls(msg.walls)
                    self.currentroom.makewalls(msg.fill, CHAR['water'])
                    self.drawmsg('You have sprung a Trap!! The room has filled with water and you drowned! (Press a Key)')
                    self.drawroom()
                    return False

                else:  # Have necklace, nothing happens
                    self.clearplayer()
                    self.y, self.x = newy, newx
                    self.drawplayer()

            elif isinstance(msg, Object):
                # Ran in to Object, pick it up if possible
                # moving player to that position
                if self.checkinv():
                    msg.loc = 'inv'
                    self.currentroom.map[newx][newy] = None
                    self.clearplayer()
                    self.y, self.x = newy, newx
                    self.drawplayer()
                else:
                    self.drawmsg("Can't carry any more!")

            elif msg in UPDN:
                if msg == CHAR['up']:
                    self.roomx, self.roomy, self.roomz = self.currentroom.exits.get(CHAR['up'], (self.roomx, self.roomy, self.roomz+1))
                else:
                    self.roomx, self.roomy, self.roomz = self.currentroom.exits.get(CHAR['dn'], (self.roomx, self.roomy, self.roomz-1))
                self.currentroom = self.rooms[self.roomx][self.roomy][self.roomz]
                self.monster = [obj for obj in self.objects.values() if isinstance(obj, Monster) and obj.loc == self.currentroom]
                self.monster = self.monster[0] if len(self.monster) > 0 else None
                self.drawroom()
                self.drawitems()
                self.drawplayer()

            elif msg is not None:
                # Got a collision message, show it
                self.drawmsg(msg)
        return True

    def cmd(self, cmd):
        ''' Process command such as GET KEY '''
        msg = 'What?'
        wrap = True
        if cmd == 'INV':
            msg = '- Inventory -\n'
            for i, item in enumerate([obj for obj in self.objects.values() if obj.loc == 'inv']):
                msg += str(item) + '\n'
            wrap = False

        elif cmd == 'QUIT':
            return 'QUIT'

        elif ' ' in cmd:
            verb, obj, *_ = cmd.split()

            # First, try interacting with room. This will allow rooms to override object actions
            msg = self.currentroom.interact(verb, obj, xy=(self.x, self.y), objects=self.objects)
            if msg is None:
                # If room didn't respond, interact with object
                item = self.objects.get(obj)
                if item is not None:
                    # Interact with object
                    msg = item.interact(verb, room=self.currentroom, xy=(self.x, self.y))

                else:
                    # fall back on default actions
                    if verb == 'LOOK':
                        if obj in ['FLOOR', 'WALL', 'CEILING']:
                            msg = 'The {}S are made of Gray Stone.'.format(obj)
                        else:
                            msg = "I can't see a {} and I don't have a {}!".format(obj, obj)

                    elif verb == 'DRINK':
                        if obj in ['WATER', 'FLASK']:
                            msg = self.drink()
                        else:
                            msg = "You don't have any {}".format(obj)

                    else:
                        msg = {'JUMP': 'I Jumped. This is boring.',
                               'SAY': '"{}"'.format(obj),
                               'EAT': "I'm not hungry!",
                               'CLIMB': "I don't need to climb.",
                               'GET': 'Impossible!'}.get(verb, 'What?')
        self.drawmsg(msg, wrap=wrap)
        self.drawroom()
        self.drawplayer()
        self.drawcmd()  # Clear command line
        return msg

    def movemonster(self):
        ''' Process event moving monster toward player '''
        assert self.monster is not None
        dx = self.monster.x - self.x
        dy = self.monster.y - self.y
        if abs(dx) > abs(dy):
            newx = self.monster.x-1 if dx > 0 else self.monster.x+1
            newy = self.monster.y
        else:
            newx = self.monster.x
            newy = self.monster.y-1 if dy > 0 else self.monster.y+1
        if newx == self.x and newy == self.y:
            if self.objects['HELMET'].loc == 'inv':
                self.health -= 2
            else:
                self.health -= 1  # Orig game the with or without the helmet you die in about 16 hits...

            if self.health < 0:
                return 'DEAD'
            else:
                self.win['msg'].clear()
                self.drawmsg('The {} struck you!{}'.format(self.monster.name, ' The helmet helps.' if self.objects['HELMET'].loc == 'inv' else ''))

        elif self.currentroom.map[newx][newy] is None:
            self.clearmonster()
            self.monster.move(newx, newy)
            self.drawmonster()
        #else: # monster is stuck behind something!

    def drawroom(self):
        ''' Draw the room in the window '''
        self.win['box'].box()
        self.currentroom.draw(self.win['room'])
        self.currentroom.drawdesc(self.win['desc'])
        self.drawitems()

    def drawplayer(self):
        ''' Draw the player icon '''
        self.win['room'].addstr(self.y, self.x*widthmultiple, CHAR['player'])

    def clearplayer(self):
        ''' Clear the player icon '''
        self.win['room'].addstr(self.y, self.x*widthmultiple, ' '*widthmultiple)

    def drawitems(self):
        ''' Draw list of items in room '''
        self.win['objs'].clear()
        self.currentroom.drawitems(self.win['objs'])

    def drawmsg(self, msg, width=14, wrap=True):
        ''' Write a message '''
        self.win['msg'].clear()
        if wrap:
            self.win['msg'].addstr(0, 0, textwrap.fill(msg, width))
        else:
            self.win['msg'].addstr(0, 0, msg)

    def drawcmd(self, cmd=''):
        ''' Write out command from keyboard '''
        self.win['cmd'].addstr(0, 0, '?'+cmd[:13], clearline=True)

    def drawmonster(self):
        self.win['room'].addstr(self.monster.y, self.monster.x*widthmultiple, self.monster.char)

    def clearmonster(self):
        self.win['room'].addstr(self.monster.y, self.monster.x*widthmultiple, ' '*widthmultiple)

    # Action callbacks
    def getitem(self, objname, xy=None, newactions=None):
        ''' Get item (using GET command, not from colliding with object)

            objname: name of object in objects dict

            xy: (optional) position within the room allowed to get the object
                x >= xy[0], x <= xy[1], y >= xy[2], y <= xy[3]

            newactions: dictionary of dictionaries defining changed room actions
                roomname: {action: newmessage}
        '''
        if self.checkinv():
            if self.objects[objname].loc == 'inv':
                msg = 'You already have it!'
            elif self.objects[objname].loc is None:  # Not picked up yet
                if xy is None or (self.x >= xy[0] and self.x <= xy[1] and self.y >= xy[2] and self.y <= xy[3]):
                    self.objects[objname].loc = 'inv'
                    msg = 'Done.'
                    if newactions:
                        for roomname in newactions.keys():
                            room = self.getroom(roomname)
                            room.actions.update(newactions[roomname])
                else:
                    msg = "I can't reach it!"
            elif self.objects[objname].loc == self.currentroom:
                msg = 'Get it yourself!'
            else:
                msg = "I don't see a {}".format(objname)
        else:
            msg = "Can't carry any more!"
        return msg

    def getwater(self):
        ''' Fill the flask '''
        if self.objects['FLASK'].loc == 'inv':
            if self.currentroom.name == 'fountain':
                if self.x >= 9 and self.x <=13 and self.y >=7 and self.y <= 11:
                    self.objects['FLASK'].full = True
                    msg = 'Done.'
                else:
                    msg = "I can't reach it!"
            else:
                msg = 'Impossible!'
        else:
            msg = "You don't have a container!"
        return msg

    def drink(self):
        ''' Drink from flask '''
        if self.objects['FLASK'].loc == 'inv' and self.objects['FLASK'].full:
            if self.health < 15:
                msg = 'That was good. I feel much better!'
                self.health = 15
            else:
                msg = 'That was good.'
        else:
            msg = "You don't have any water!"
        return msg
    
    def readbook(self):
        ''' Read the book, if have glasses '''
        if self.objects['GLASSES'].loc != 'inv':
            msg = "You can't see well enough. It's all Blurry."
        else:
            msg = "The book reads: Wave Scepter"
        return msg

    def showcross(self):
        ''' Show the holy cross '''
        if self.currentroom.name == 'vampire' and self.objects['VAMPIRE'].loc == self.currentroom:
            msg = 'The vampire sees the holy cross and disappears!'
            self.objects['VAMPIRE'].loc = None
            self.currentroom.map[rgtx][9] = None
        else:
            msg = "There's not anyone to show it to!"
        return msg
    
    def wavewand(self):
        if self.currentroom.name == 'sorcerer':
            msg = 'As you wave the WAND....\n\nThere is a *Puff* of smoke Revealing a Secret passage!'
            self.currentroom = self.getroom('sorcereropen')
            self.roomx += 1
        elif self.currentroom.name == 'winding':
            msg = 'As you wave the WAND....\n\nThere is a *Puff* of smoke Revealing a Secret passage!'
            self.currentroom.map[11][17] = None
            self.currentroom.map[12][17] = None
        else:
            msg = 'You wave the WAND....\n\nNothing Happens!'
        return msg

    def sorcback(self):
        ''' Go back to sorcerers room '''
        self.currentroom = self.getroom('sorcerer')
        self.roomx -= 2
        self.y = 8
        self.x = 12
        msg = 'There is a puff of smoke & you appear in the Sorcerers room!'
        self.drawroom()
        self.drawplayer()
        return msg

    def opendoor(self):
        ''' Open the door to dungeon if you have key '''
        if self.objects['KEY'].loc == 'inv':
            self.currentroom.map[13][8] = self.currentroom.map[13][9] = None
            msg = 'Done.'
        else:
            msg = "You don't have a key!"
        return msg

    def opendoor2(self):
        ''' Open the door to dungeon if you have key '''
        if self.objects['KEY'].loc == 'inv':
            self.currentroom.map[8][14] = self.currentroom.map[9][14] = None
            msg = 'Done.'
        else:
            msg = "You don't have a key!"
        return msg

    def playharp(self):
        ''' Play the harp '''
        if self.objects['HARP'].loc == 'inv':
            msg = 'The Harp makes Beautiful Music!\n'
            if self.objects['FAIRY'].loc is not None and self.currentroom.name in ['fairy', 'fairy2']:
                room1 = self.getroom('fairy')
                room2 = self.getroom('fairy2')
                room1.map[0][9] = None
                room2.map[11][0] = None
                self.objects['FAIRY'].loc = None
                msg += 'The fairy likes the music and leaves!'
        else:
            msg = "You don't have it!"
        return msg

    def toodark(self):
        ''' Can't go in the dungeon without lamp '''
        if self.objects['LAMP'].loc == 'inv':
            return CHAR['dn'] 
        else:
            return "It's too dark to go that way!"

    def wavescepter(self):
        ''' Open the front door '''
        msg = 'As you wave the SCEPTER....\nNothing Happens!'
        if self.currentroom.name == 'entrance':
            msg = 'As you wave the SCEPTER....\nThe Gate opens by itself!'            
            self.currentroom.map[10][17] = self.currentroom.map[11][17] = None
            self.currentroom.map[12][17] = self.currentroom.map[13][17] = None
        return msg

    def makerooms(self):
        ''' Lay out the castle '''
        floor = 1
        r = Room('You are in the Castle Courtyard. To the north is a large Doorway. To the south is a large gate.', 'entrance')
        r.makewalls([[9, 0], [0, 0], [0, boty], [9, boty]])
        r.makewalls([[14, 0], [rgtx, topy], [rgtx, boty], [14, boty]])
        r.makewalls([[10, boty], [13, boty]], char=CHAR['gate'], collide='The Gate is locked.')
        r.actions[('LOOK', 'GATE')] = 'It looks very Strong.'
        r.actions[('OPEN', 'GATE')] = 'The Gate is locked.'
        self.rooms[1][0][1] = r

        r = Room('You are in the Entrance room. Exits are to the north & south.')
        r.makewalls([[9,0],[9,3],[0,3],[0,boty-3],[9,boty-3],[9,boty]])
        r.makewalls([[14,0],[14,3],[rgtx,3],[rgtx,boty-3],[14,boty-3],[14,boty]])
        self.rooms[1][1][floor] = r

        r = Room('You are in The Welcome Hall. This room was used to welcome guests. There are large archways in all four walls.')
        r.makewalls([[9,0], [9,1], [1,1], [1,6], [0,6]])
        r.makewalls([[14,0], [14,1], [rgtx-1,1], [rgtx-1,6], [rgtx,6]])
        r.makewalls([[9,boty], [9,boty-1], [1,boty-1], [1,11], [0,11]])
        r.makewalls([[14,boty], [14,boty-1], [rgtx-1,boty-1], [rgtx-1,11], [rgtx,11]])
        self.rooms[1][2][floor] = r

        r = Room('You are in The West Ballroom. There are arch ways to the north & east; a spiral staircase in one corner.')
        r.makewalls([[rgtx,6],[rgtx-1,6],[rgtx-1,1],[14,1],[14,0]])
        r.makewalls([[9,0], [9,1],[1,1],[1,boty],[rgtx-1,boty],[rgtx-1,boty-6],[rgtx,boty-6]])
        r.map[rgtx-3][3] = TILE_UP
        self.rooms[0][2][floor] = r
        self.objects['OGRE'] = Monster(12, 12, r, CHAR['ogre'], 'OGRE', '', adjective='UGLY')

        r = Room('You are in The East Ballroom. There are arch ways to the north & west; a spiral staircase in one corner.')
        r.makewalls([[0,6],[1,6],[1,1],[9,1],[9,0]])
        r.makewalls([[14,0], [14,1],[22,1],[22,boty],[1,boty],[1,boty-6],[0,boty-6]])
        r.map[3][3] = TILE_UP
        self.rooms[2][2][floor] = r

        r = Room('You are in The Central Hall. Exits are in all directions. There is a large spiral staircase in the middle.')
        r.makewalls([[9,0],[9,1],[1,1],[1,6],[0,6]])
        r.makewalls([[14,0],[14,1],[rgtx-1,1],[rgtx-1,6],[rgtx,6]])
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,11],[0,11]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-1,boty-1],[rgtx-1,11],[rgtx,11]])
        r.map[11][8] = TILE_UP
        r.map[12][8] = TILE_UP
        r.map[11][9] = TILE_UP
        r.map[12][9] = TILE_UP
        self.rooms[1][3][floor] = r

        r = Room('You are in the West Dining room. There 2 door ways to the north, & arch ways to the east & south.')  # sic
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,1],[4,1],[4,0]])
        r.makewalls([[7,0],[7,1],[16,1],[16,0]])
        r.makewalls([[19,0],[19,1],[22,1],[22,6],[rgtx,6]])
        r.makewalls([[rgtx,11],[rgtx-1,11],[rgtx-1,boty-1],[14,boty-1],[14,boty]])
        self.rooms[0][3][floor] = r
        self.objects['GOBLET'] = Object(5, 12, r, CHAR['goblet'], 'GOBLET', "It's made of Gold!", adjective='FANCY')

        r = Room('You are in The East Dining room. The large opening to the east leads to the garden patio.')
        r.makewalls([[9,0],[9,1],[1,1],[1,6],[0,6]])
        r.makewalls([[14,0],[14,1],[rgtx-1,1],[rgtx-1,4],[rgtx,4]])
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,11],[0,11]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-1,boty-1],[rgtx-1,13],[rgtx,13]])
        self.rooms[2][3][floor] = r

        r = Room('You are in The Castle Museum. This room was once decorated with many artifacts.')
        r.makewalls([[9,boty],[9,boty-1],[7,boty-1],[7,1],[rgtx-2,1],[rgtx-2,5],[rgtx,5]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-2,boty-1],[rgtx-2,boty-4],[rgtx,boty-4]])
        self.rooms[2][4][floor] = r
        self.objects['SWORD'] = Object(9, 4, r, CHAR['sword'], 'SWORD', 'It looks sharp!', score=False)

        r = Room('You are In The North end of the Castle Garden. It is overgrown with bushes.')
        r.makewalls([[0,5],[2,5],[2,1],[rgtx-1,1],[rgtx-1,boty]])
        r.makewalls([[0,boty-4],[2,boty-4],[2,boty]])
        r.map[5][13] = r.map[13][13] = r.map[19][14] = r.map[14][boty-1] = Tile(CHAR['shrub1'])
        r.map[18][4] = r.map[18][7] = r.map[7][7] = r.map[13][9] = Tile(CHAR['shrub2'])
        r.map[8][3] = r.map[14][5] = r.map[12][6] = r.map[10][9] = r.map[11][11] = r.map[18][11] = Tile(CHAR['shrub1'])
        self.rooms[3][4][floor] = r
        self.objects['LAMP'] = Object(18, 13, r, CHAR['lamp'], 'LAMP', "It's lit Magically", score=False)
        self.objects['LAMP'].actions['RUB'] = lambda: "There's no Genii in this Lamp!"
        self.objects['SNAKE'] = Monster(16, 6, r, CHAR['snake'], 'SNAKE', '')

        r = Room('You are in The South end of The Castle garden. In the center is a fountain.', 'fountain')
        r.makewalls([[0,4],[2,4],[2,0]])
        r.makewalls([[0,13],[2,13],[2,boty-1],[rgtx-1,boty-1],[rgtx-1,0]])
        r.map[3][0] = r.map[7][4] = r.map[7][11] = r.map[17][5] = r.map[15][boty-3] = Tile(CHAR['shrub1'])
        r.map[9][boty-4] = r.map[18][10] = r.map[11][2] = Tile(CHAR['shrub2'])
        r.map[11][9] = Tile(CHAR['fount'])
        if not emoji:
            r.map[10][8] = Tile(CHAR['boxUL'])
            r.map[11][8] = Tile(CHAR['hline'])
            r.map[12][8] = Tile(CHAR['boxUR'])
            r.map[10][9] = Tile(CHAR['vline'])
            r.map[12][9] = Tile(CHAR['vline'])
            r.map[10][10] = Tile(CHAR['boxLL'])
            r.map[11][10] = Tile(CHAR['hline'])
            r.map[12][10] = Tile(CHAR['boxLR'])
        r.actions[('LOOK', 'FOUNTAIN')] = "The fountain is filled with water. But you can't see In it."
        r.actions[('GET', 'WATER')] = self.getwater
        r.actions[('FILL', 'FLASK')] = self.getwater
        r.actions[('GET', 'GEM')] = lambda: self.getitem('GEM', (9,13,7,11), {'balcony': {('LOOK','GARDEN'):'The Garden Has A Fountain in the middle.'}})
        self.rooms[3][3][floor] = r

        r = Room('You are in the Ante Room. Here People waited for an audience with the King. It was once lined with benches.')
        r.makewalls([[9,0],[9,5],[6,5],[6,12],[9,12],[9,boty]])
        r.makewalls([[14,0],[14,5],[17,5],[17,12],[14,12],[14,boty]])
        self.rooms[1][4][floor] = r

        r = Room('You are in the Throne Room. There is a Large Throne at one end of the room.')
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,5],[0,5]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-1,boty-1],[rgtx-1,5],[rgtx,5]])
        r.makewalls([[0,3],[1,3],[1,2],[rgtx-1,2],[rgtx-1,3],[rgtx,3]])
        r.makewalls([[10,3],[10,4]], char=CHAR['solid'])
        r.makewalls([[13,3],[13,4]], char=CHAR['solid'])
        r.makewalls([[11,4],[12,4]], char=CHAR['hline'])
        self.objects['CROWN'] = Object(8, 4, r, CHAR['crown'], 'CROWN', "It's made of Gold!")
        self.objects['CROWN'].actions['WEAR'] = lambda: 'The crown is too Heavy!'
        r.actions[('GET', 'THRONE')] = 'THRONES are too heavy!'
        self.objects['DEMON'] = Monster(14, 5, r, CHAR['demon'], 'DEMON', '', adjective='ANGRY')
        self.rooms[1][5][floor] = r

        r = Room('You are in the Kings Dressing room. It was once filled with clothes. There is a staircase in one corner.')
        r.makewalls([[rgtx,3],[rgtx-1,3],[rgtx-1,0],[rgtx-17,0],[rgtx-17,8],[rgtx-1,8],[rgtx-1,5],[rgtx,5]])
        r.map[rgtx-3][6] = TILE_UP
        self.rooms[0][5][floor] = r

        r = Room("You are in the Queen's Dressing room. It was once filled with clothes. There is a staircase in one corner.")
        r.makewalls([[0,3],[1,3],[1,0],[17,0],[17,8],[1,8],[1,5],[0,5]])
        r.map[3][6] = TILE_UP
        self.rooms[2][5][floor] = r

        r = Room('You are in The Kitchen. In the Center is a large stone table.')
        r.makewalls([[rgtx-4,boty],[rgtx-4,boty-2],[rgtx-2,boty-2],[rgtx-2,5],[rgtx,5]])
        r.makewalls([[rgtx,3],[rgtx-2,3],[rgtx-2,2],[13,2],[13,0]])
        r.makewalls([[10,0],[10,2],[3,3],[3,boty-2],[4,boty-2],[4,boty]])
        r.makewalls([[7,boty],[7,boty-2],[16,boty-2],[16,boty]])
        r.makewalls([[8,8],[16,8],[16,9],[8,9]], char=CHAR['solid'])
        r.actions[('LOOK', 'TABLE')] = "It's made of Stone."
        r.actions[('GET', 'TABLE')] = "TABLES are too heavy!"
        r.exits['E'] = 0,0,floor  # Conflict with "Ante room"
        r.exits['N'] = 0,1,floor  # Conflict with "King's dressing room"
        self.rooms[0][4][floor] = r

        r = Room("You are in The Chef's Quarters. There is a small desk & a Bed here.")
        r.makewalls([[0,3],[2,3],[2,0],[12,0],[12,13],[2,13],[2,5],[0,5]])
        r.makewalls([[9,1],[11,1]], char=CHAR['solid'])
        r.map[6][10] = Tile(CHAR['boxUL'])
        r.map[9][10] = Tile(CHAR['boxUR'])
        r.map[6][11] = Tile(CHAR['boxLL'])
        r.map[9][11] = Tile(CHAR['boxLR'])
        r.map[7][10] = r.map[8][10] = r.map[7][11] = r.map[8][11] = Tile(CHAR['hline'])
        r.exits['W'] = 0,4,floor  # Go back to Kitchen
        r.actions[('LOOK', 'BED')] = "It's Old."
        r.actions[('LOOK', 'DESK')] = 'There is a Wine Flask on top'
        r.actions[('GET', 'FLASK')] = lambda: self.getitem('FLASK', (5,11,9,100), {'current': {('LOOK','DESK'): "It's made of wood."}})
        self.rooms[0][0][floor] = r  # Nothing in this corner of the castle

        r = Room('You are in the Storage room. There are Two large shelves in the middle,and a Small Staircase in one corner.')
        r.makewalls([[10,boty],[10,boty-3],[0,boty-3],[0,4],[18,4],[18,boty-3],[13,boty-3],[13,boty]])
        r.makewalls([[6,7],[14,7]], char=CHAR['solid'])
        r.makewalls([[6,9],[14,9]], char=CHAR['solid'])
        r.map[2][6] = Tile(CHAR['dn'])
        r.map[2][6].collide=self.toodark
        r.actions[('LOOK', 'SHELF')] = "They're made of wood"
        r.actions[('LOOK', 'SHELVES')] = r.actions[('LOOK', 'SHELF')]
        r.actions[('GET', 'SHELF')] = 'SHELFS are too heavy!'
        r.exits['S'] = 0,4,floor  # Go back to kitchen
        r.exits[CHAR['dn']] = 0, 5, 0  # Dungeon
        self.rooms[0][1][floor] = r  # Nothing in this corner of the castle

        # SECOND FLOOR
        floor = 2
        r = Room('You are in The Anex Hall. There is a large spiral staircase in the middle.')
        r.makewalls([[10,0],[10,1],[1,1],[1,6],[0,6]])
        r.makewalls([[13,0],[13,1],[rgtx-1,1],[rgtx-1,6],[rgtx,6]])
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,11],[0,11]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-1,boty-1],[rgtx-1,11],[rgtx,11]])
        r.map[11][8] = TILE_DN
        r.map[12][8] = TILE_DN
        r.map[11][9] = TILE_DN
        r.map[12][9] = TILE_DN
        self.rooms[1][3][floor] = r

        r = Room('You are in the west end of the Upper hall. There is a staircase leading down.')
        r.makewalls([[rgtx,1],[rgtx-9,1],[rgtx-9,0]])
        r.makewalls([[9,0],[9,1],[1,1],[1,boty-1],[rgtx,boty-1]])
        r.map[rgtx-3][3] = TILE_DN
        self.rooms[0][2][floor] = r
        self.objects['HOURGLASS'] = Object(4, boty-4, r, CHAR['hourglass'], 'HOURGLASS', "It's made of gold!")

        r = Room('You are in the center of The upper Hall. There is a statue of the King in the middle.')
        r.makewalls([[0, boty-1],[rgtx,boty-1]])
        r.makewalls([[0,1],[9,1],[9,0]])
        r.makewalls([[14,0],[14,1],[rgtx,1]])
        r.map[11][8] = Tile(CHAR['statue'])
        if not emoji:
            r.map[10][7] = Tile(CHAR['boxUL'])
            r.map[11][7] = Tile(CHAR['hline'])
            r.map[12][7] = Tile(CHAR['boxUR'])
            r.map[10][8] = Tile(CHAR['vline'])
            r.map[12][8] = Tile(CHAR['vline'])
            r.map[10][9] = Tile(CHAR['boxLL'])
            r.map[11][9] = Tile(CHAR['hline'])
            r.map[12][9] = Tile(CHAR['boxLR'])
        r.actions[('GET', 'NECKLACE')] = lambda: self.getitem('NECKLACE', (9,13,6,10), {'current': {('LOOK','STATUE'):'The Statue looks like The King!'}})
        r.actions[('LOOK', 'STATUE')] = 'The statue is wearing a Necklace'
        r.actions[('GET', 'STATUE')] = 'STATUES are too heavy!'
        self.rooms[1][2][floor] = r

        r = Room('You are in the east end of the Upper hall. There is a staircase leading down.')
        r.makewalls([[0,1],[9,1],[9,0]])
        r.makewalls([[14,0],[14,1],[rgtx-1,1],[rgtx-1,boty-1],[0,boty-1]])
        r.map[3][3] = TILE_DN
        self.rooms[2][2][floor] = r
        self.objects['OGRE2'] = Monster(14, 4, r, CHAR['ogre'], 'OGRE', '', adjective='UGLY')

        r = Room('You are in The Guards Hall. This room was once used to honor Members of the Royal Guard.')
        r.makewalls([[rgtx-6,0],[rgtx-6,1],[1,1],[1,6],[0,6]])
        r.makewalls([[rgtx-3,0],[rgtx-3,1],[rgtx-1,1],[rgtx-1,6],[rgtx,6]])
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,11],[0,11]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-1,boty-1],[rgtx-1,11],[rgtx,11]])
        self.rooms[2][3][floor] = r
        self.objects['HELMET'] = Object(11, 4, r, CHAR['helmet'], 'HELMET', 'It looks Tough!', score=False)
        self.objects['HELMET'].actions['WEAR'] = lambda: "Ok, I'm wearing it."
        self.rooms[2][3][floor] = r

        r = Room('You are on a Balcony looking over the Castle Gardens.', 'balcony')
        r.makewalls([[0,6],[1,6],[1,1],[6,1]])
        r.makewalls([[0,11],[1,11],[1,boty-1],[6,boty-1]])
        r.map[7][1] = r.map[7][boty-1] = TILE_SOLID
        r.map[8][1] = r.map[8][boty-1] = Tile(CHAR['hline'])
        r.map[9][1] = Tile(CHAR['boxUR'])
        r.map[9][boty-1] = Tile(CHAR['boxLR'])
        r.map[9][2] = r.map[9][4] = r.map[9][6] = r.map[9][7] = r.map[9][10] = r.map[9][11] = r.map[9][13] = r.map[9][15] = Tile(CHAR['vline'])
        r.map[9][3] = r.map[9][5] = r.map[9][8] = r.map[9][9] = r.map[9][12] = r.map[9][14] = Tile(CHAR['teeR'])
        self.rooms[3][3][floor] = r
        r.actions[('LOOK','GARDEN')] = r.actions[('LOOK','GARDENS')] = 'There is a BIG Gem in the Garden Fountain!'
        r.actions[('LOOK','BALCONY')] = 'The balcony is made of stone'

        r = Room('You are in a lower Battlement. There is a staircase at one end.')
        r.makewalls([[rgtx-3,boty],[rgtx-3,boty-2],[rgtx-1,boty-2],[rgtx-1,0],
                     [rgtx-8,0],[rgtx-8,boty-2],[rgtx-6,boty-2],[rgtx-6,boty]])
        r.map[rgtx-4][2] = r.map[rgtx-5][2] = TILE_UP
        r.exits[CHAR['up']] = 2,4,5  # Jump a couple floors
        self.rooms[2][4][floor] = r

        r = Room("You are in The Knights Hall. This room was once used to Honor The King's Knights.")
        r.makewalls([[9,boty],[9,boty-1],[1,boty-1],[1,1],[3,1],[3,0]])
        r.makewalls([[6,0],[6,1],[rgtx-1,1],[rgtx-1,6],[rgtx,6]])
        r.makewalls([[14,boty],[14,boty-1],[rgtx-1,boty-1],[rgtx-1,11],[rgtx,11]])
        self.objects['WAND'] = Object(3, 7, r, CHAR['wand'], 'WAND', 'It looks Magical!', score=False)
        self.objects['WAND'].actions['WAVE'] = self.wavewand
        self.rooms[0][3][floor] = r
        self.objects['DEMON2'] = Monster(4, 5, r, CHAR['demon'], 'DEMON', '', adjective='ANGRY')

        r = Room('You are in a lower Battlement. There is a staircase at one end.')
        r.makewalls([[6,boty],[6,boty-2],[8,boty-2],[8,0],
                     [1,0],[1,boty-2],[3,boty-2],[3,boty]])
        r.map[4][2] = r.map[5][2] = TILE_UP
        r.exits[CHAR['up']] = 1,4,5
        self.objects['BARS'] = Object(5, 5, r, CHAR['bars'], 'BARS', 'They look Expensive!', adjective='SILVER')
        self.rooms[0][4][floor] = r

        r = Room('You are in a Cooridor.', 'vampire')
        r.makewalls([[10,boty],[10,boty-1],[9,boty-1],[9,10],[0,10]])
        r.makewalls([[13,boty],[13,boty-1],[14,boty-1],[14,10],[rgtx,10]])
        r.makewalls([[0,7],[9,7],[9,0]])
        r.makewalls([[14,0],[14,7],[rgtx,7],[rgtx,8]])
        self.objects['VAMPIRE'] = Object(rgtx, 9, r, CHAR['vampire'], 'VAMPIRE', '')
        self.objects['VAMPIRE'].collide = lambda: "The Vampire is blocking your way. He can't be hurt."
        r.actions[('LOOK', 'VAMPIRE')] = 'The VAMPIRE looks Mean!'
        r.exits['E'] = 2,0,floor  # Room conflict with battlements
        r.exits['W'] = 0,0,floor
        self.rooms[1][4][floor] = r

        r = Room('You are In a Guest Room.')
        r.makewalls([[0,8],[0,7],[3,7],[3,3],[20,3],[20,14],[3,14],[3,10],[0,10]])
        self.objects['FIGURINE'] = Object(12, 7, r, CHAR['figurine'], 'FIGURINE', 'It looks Expensive!', adjective='JADE')
        r.exits['W'] = 1,4,floor
        self.rooms[2][0][floor] = r

        r = Room('You are In a Guest Room.')
        r.makewalls([[rgtx,7],[rgtx-3,7],[rgtx-3,3],[3,3],[3,14],[rgtx-3,14],[rgtx-3,10],[rgtx,10]])
        r.exits['E'] = 1,4,floor
        self.rooms[0][0][floor] = r

        r = Room('You are In a Cooridor. There is a staircase at one end.')
        r.makewalls([[9,boty],[9,10],[0,10]])
        r.makewalls([[0,7],[9,7],[9,1],[14,1],[14,7],[rgtx,7]])
        r.makewalls([[14,boty],[14,10],[rgtx,10]])
        r.map[11][3] = r.map[12][3] = TILE_UP
        self.rooms[1][5][floor] = r

        r = Room('You are In the Kings room. It was once Furnished but is now empty. There is a staircase in one corner.')
        r.makewalls([[rgtx,7],[rgtx-1,7],[rgtx-1,4],[3,4],[3,13],[rgtx-1,13],[rgtx-1,10],[rgtx,10]])
        r.map[rgtx-3][6] = TILE_DN
        self.rooms[0][5][floor] = r

        r = Room('You are In the Queens Bed room. It was once Furnished but is now empty. There is a staircase in one corner.')
        r.makewalls([[0,7],[1,7],[1,4],[17,4],[17,13],[1,13],[1,10],[0,10]])
        r.map[3][6] = TILE_DN
        self.rooms[2][5][floor] = r

        # THIRD floor
        floor = 3
        r = Room('You are In a Cooridor. There is a staircase at one end.')
        r.makewalls([[9,boty],[9,10],[0,10]])
        r.makewalls([[0,7],[9,7],[9,1],[14,1],[14,7],[rgtx,7]])
        r.makewalls([[14,boty],[14,10],[rgtx,10]])
        r.map[11][3] = r.map[12][3] = TILE_DN
        self.rooms[1][5][floor] = r

        r = Room('You are In a cooridor. There is a staircase at one end.')
        r.makewalls([[9,0],[9,7],[0,7]])
        r.makewalls([[0,10],[9,10],[9,13],[14,13],[14,10],[rgtx,10]])
        r.makewalls([[14,0],[14,7],[rgtx,7]])
        r.map[11][11] = r.map[12][11] = TILE_UP
        self.rooms[1][4][floor] = r

        r = Room('You are In a cooridor.')
        r.makewalls([[rgtx,10],[rgtx-10,10],[rgtx-10,boty]])
        r.makewalls([[rgtx,7],[rgtx-10,7],[rgtx-10,0]])
        r.makewalls([[rgtx-13,0],[rgtx-13,7],[rgtx-18,7],[rgtx-18,10],[rgtx-13,10],[rgtx-13,boty]])
        self.rooms[0][4][floor] = r

        r = Room('You are in The Red Room. This is decorated all in red. It is mostly empty now except for 2 beds.')
        r.makewalls([[10,0],[10,3],[3,3],[3,14],[rgtx-3,14],[rgtx-3,3],[13,3],[13,0]])
        r.makewalls([[6,8],[6,10],[7,10],[7,8]], char=CHAR['solid'])
        r.makewalls([[16,8],[16,10],[17,10],[17,8]], char=CHAR['solid'])
        r.actions[('LOOK','BED')] = "It's red."
        r.actions[('GET','BED')] = 'BEDS are too heavy!'
        self.objects['CROSS'] = Object(14, 12, r, CHAR['cross'], 'CROSS', "It's made of gold!", adjective='HOLY')
        self.objects['CROSS'].actions['SHOW'] = self.showcross
        self.rooms[0][3][floor] = r

        r = Room('You are in the Purple room. It is Decorated all in purple. There is 1 bed left.')
        r.makewalls([[rgtx,7],[rgtx-2,7],[rgtx-2,3],[3,3],[3,14],[10,14],[10,boty]])
        r.makewalls([[13,boty],[13,14],[rgtx-2,14],[rgtx-2,10],[rgtx,10]])
        r.makewalls([[5,5],[8,5],[8,6],[5,6]], char=CHAR['solid'])
        self.rooms[0][5][floor] = r

        r = Room('You are In a cooridor.')
        r.makewalls([[0,10],[10,10],[10,boty]])
        r.makewalls([[0,7],[10,7],[10,0]])
        r.makewalls([[13,0],[13,7],[18,7],[18,10],[13,10],[13,boty]])
        self.rooms[2][4][floor] = r
    
        r = Room('You are in The Blue Room. This is decorated all in blue. It is mostly empty now except for 2 beds.')
        r.makewalls([[10,0],[10,3],[3,3],[3,14],[rgtx-3,14],[rgtx-3,3],[13,3],[13,0]])
        r.makewalls([[6,8],[6,10],[7,10],[7,8]], char=CHAR['solid'])
        r.makewalls([[16,8],[16,10],[17,10],[17,8]], char=CHAR['solid'])
        r.actions[('LOOK','BED')] = "It's blue."
        r.actions[('GET','BED')] = 'BEDS are too heavy!'
        self.rooms[2][3][floor] = r

        r = Room('You are in the Yellow room. It is Decorated all in Yellow. Now it is empty except for a desk & bed.')
        r.makewalls([[0,7],[2,7],[2,3],[rgtx-3,3],[rgtx-3,14],[rgtx-10,14],[rgtx-10,boty]])
        r.makewalls([[rgtx-13,boty],[rgtx-13,14],[2,14],[2,10],[0,10]])
        r.makewalls([[5,5],[8,5],[8,6],[5,6]], char=CHAR['solid'])
        r.map[16][11] = Tile(CHAR['boxUL'])
        r.map[17][11] = Tile(CHAR['hline'])
        r.map[18][11] = Tile(CHAR['boxUR'])
        r.map[16][12] = Tile(CHAR['boxLL'])
        r.map[17][12] = Tile(CHAR['hline'])
        r.map[18][12] = Tile(CHAR['boxLR'])
        r.actions[('LOOK','DESK')] = 'There is a Pair of Eye Glasses on Top.'
        r.actions[('LOOK','BED')] = "It's Yellow."
        r.actions[('GET','BED')] = 'BEDS are too heavy!'
        r.actions[('GET','GLASSES')] = lambda: self.getitem('GLASSES', (14,100,13,100), {'current': {('LOOK','DESK'): "It's made of wood"}})
        self.rooms[2][5][floor] = r

        # FOURTH floor
        floor = 4
        r = Room('You are In a cooridor. There is a staircase going down.')
        r.makewalls([[9,0],[9,7],[0,7]])
        r.makewalls([[0,10],[9,10],[9,13],[14,13],[14,10],[rgtx,10]])
        r.makewalls([[14,0],[14,7],[rgtx,7]])
        r.map[11][11] = r.map[12][11] = TILE_DN
        self.rooms[1][4][floor] = r

        r = Room('You are In a cooridor.')
        r.makewalls([[0,10],[10,10],[10,boty]])
        r.makewalls([[0,7],[10,7],[10,0]])
        r.makewalls([[13,0],[13,7],[18,7],[18,10],[13,10],[13,boty]])
        self.rooms[2][4][floor] = r

        r = Room("You are in the King's study. A large Wooden Desk is all that is left.")
        r.makewalls([[0,7],[2,7],[2,3],[rgtx-3,3],[rgtx-3,14],[rgtx-10,14],[rgtx-10,boty]])
        r.makewalls([[rgtx-13,boty],[rgtx-13,14],[2,14],[2,10],[0,10]])
        r.map[9][6] = Tile(CHAR['boxUL'])
        r.map[14][6] = Tile(CHAR['boxUR'])
        r.map[9][8] = Tile(CHAR['boxLL'])
        r.map[14][8] = Tile(CHAR['boxLR'])
        r.map[9][7] = r.map[14][7] = Tile(CHAR['vline'])
        r.makewalls([[10,8],[13,8]], char=CHAR['hline'])
        r.makewalls([[10,6],[13,6]], char=CHAR['hline'])
        r.actions[('LOOK','DESK')] = 'There is a Key on Top.'
        r.actions[('GET','KEY')] = lambda: self.getitem('KEY', (8,15,0,9), {'current':{('LOOK','DESK'): "It's made of wood."}})
        self.rooms[2][5][floor] = r

        r = Room('You are in a Cooridor.', 'fairy')
        r.makewalls([[0,8],[0,7],[rgtx,7]])
        r.makewalls([[0,10],[9,10],[9,boty]])
        r.makewalls([[14,boty],[14,10],[rgtx,10]])
        self.objects['FAIRY'] = Object(0, 9, r, CHAR['fairy'], 'FAIRY', '')
        self.objects['FAIRY'].collide = lambda: "The fairy is blocking your way. He can't be hurt."
        r.actions[('LOOK', 'FAIRY')] = 'The FAIRY looks pretty but unhappy.'
        self.rooms[1][5][floor] = r

        r = Room('You are in a totally Empty room.')
        r.makewalls([[rgtx,8],[rgtx,7],[rgtx-3,7],[rgtx-3,3],[3,3],[3,14],[10,14],[10,boty]])
        r.makewalls([[rgtx,10],[rgtx-3,10],[rgtx-3,14],[13,14],[13,boty],[12,boty]])
        self.objects['DIAMOND'] = Object(11, 9, r, CHAR['diamond'], 'DIAMOND', 'It looks Expensive!')
        self.rooms[0][5][floor] = r

        r = Room('You are In a cooridor.', 'fairy2')
        r.makewalls([[rgtx,10],[rgtx-10,10],[rgtx-10,boty]])
        r.makewalls([[rgtx,7],[rgtx-10,7],[rgtx-10,0],[rgtx-11,0]])
        r.makewalls([[rgtx-13,0],[rgtx-13,7],[rgtx-18,7],[rgtx-18,10],[rgtx-13,10],[rgtx-13,boty]])
        r.map[rgtx-12][0] = self.objects['FAIRY']
        self.rooms[0][4][floor] = r

        r = Room("You are in the west end of the King's Library. It is full of shelves.")
        r.makewalls([[10,0],[10,1],[4,1],[4,boty-1],[rgtx,boty-1]])
        r.makewalls([[13,0],[13,1],[rgtx,1]])
        r.makewalls([[7,7],[13,7]], char=CHAR['solid'])
        r.makewalls([[7,10],[13,10]], char=CHAR['solid'])
        r.makewalls([[7,13],[13,13]], char=CHAR['solid'])
        r.makewalls([[17,7],[23,7]], char=CHAR['solid'])
        r.makewalls([[17,10],[23,10]], char=CHAR['solid'])
        r.makewalls([[17,13],[23,13]], char=CHAR['solid'])
        r.actions[('LOOK', 'SHELF')] = "There's a book on it."
        r.actions[('LOOK', 'SHELVES')] = "There's a book on one."
        r.actions[('GET', 'BOOK')] = lambda: self.getitem('BOOK', (6,100,6,14), {'current': {('LOOK','SHELF'): "They're made of wood.", ('LOOK', 'SHELVES'): "They're made of wood."}})
        r.exits['E'] = 2,3,floor  # The library is longer than it looks...
        self.rooms[0][3][floor] = r

        r = Room("You are in the East end of the King's Library. It is full of shelves.")
        r.makewalls([[13,0],[13,1],[19,1],[19,boty-1],[0,boty-1]])
        r.makewalls([[10,0],[10,1],[0,1]])
        r.makewalls([[0,7],[6,7]], char=CHAR['solid'])
        r.makewalls([[0,10],[6,10]], char=CHAR['solid'])
        r.makewalls([[0,13],[6,13]], char=CHAR['solid'])
        r.makewalls([[10,7],[16,7]], char=CHAR['solid'])
        r.makewalls([[10,10],[16,10]], char=CHAR['solid'])
        r.makewalls([[10,13],[16,13]], char=CHAR['solid'])
        r.actions[('LOOK', 'SHELF')] = "They're made of wood."
        r.actions[('LOOK', 'SHELVES')] = "They're made of wood."
        r.exits['W'] = 0,3,floor
        self.rooms[2][3][floor] = r

        # BATTLEMENTs. Look like floor 3, but make them 5 to not conflict with 3 and 4
        floor = 5
        r = Room('You are in An upper Battlement. There is a staircase in one corner.')
        r.makewalls([[0,0],[rgtx-1,0],[rgtx-1,boty]])
        r.makewalls([[0,4],[15,4],[15,boty]])
        r.map[18][2] = TILE_DN
        r.map[19][2] = TILE_UP
        r.exits[CHAR['dn']] = 2,4,2
        self.rooms[2][4][floor] = r

        r = Room('You are in An upper Battlement. There is a staircase in one corner.')
        r.makewalls([[rgtx,0],[1,0],[1,boty]])
        r.makewalls([[rgtx,4],[8,4],[8,boty]])
        r.map[5][2] = TILE_DN
        r.map[4][2] = TILE_UP
        r.exits[CHAR['dn']] = 0,4,2
        self.rooms[1][4][floor] = r

        r = Room('You are in An upper Battlement')
        r.makewalls([[1,0],[1,boty]])
        r.makewalls([[8,0],[8,boty]])
        self.rooms[1][3][floor] = r

        r = Room('You are in An upper Battlement. There is a long staircase leading down.')
        r.makewalls([[rgtx-1,0],[rgtx-1,6],[rgtx,6],[rgtx,11],[rgtx-1,11],[rgtx-1,boty]])
        r.makewalls([[rgtx-8,0],[rgtx-8,boty]])
        r.map[22][8] = r.map[22][9] = TILE_DN
        r.exits[CHAR['dn']] = 4,1,0  # Staircase goes to dungeon.
        self.rooms[2][3][floor] = r

        r = Room('You are in An upper Battlement')
        r.makewalls([[1,0],[1,boty],[rgtx,boty]])
        r.makewalls([[8,0],[8,13],[rgtx,13]])
        self.rooms[1][2][floor] = r

        r = Room('You are in An upper Battlement')
        r.makewalls([[rgtx-1,0],[rgtx-1,boty],[0,boty]])
        r.makewalls([[15,0],[15,13],[0,13]])
        self.rooms[2][2][floor] = r

        # WALL
        floor = 6
        r = Room('You are on the Castle Wall. There is a stair case leading down in one corner.')
        r.makewalls([[rgtx,0],[1,0],[1,boty]])
        r.makewalls([[rgtx,4],[8,4],[8,boty]])
        r.map[5][2] = TILE_DN
        r.map[4][2] = TILE_DN
        self.rooms[1][4][floor] = r

        r = Room('You are on the Castle Wall. There is a stair case leading down in one corner.')
        r.makewalls([[0,0],[rgtx-1,0],[rgtx-1,boty]])
        r.makewalls([[0,4],[15,4],[15,boty]])
        r.map[18][2] = TILE_DN
        r.map[19][2] = TILE_DN
        self.rooms[2][4][floor] = r

        r = Room('You are Below the West Tower. there is a large staircase leading up the tower.')  # sic
        r.makewalls([[1,0],[1,6],[0,6],[0,11],[1,11],[1,boty]])
        r.makewalls([[8,0],[8,6],[9,6],[9,11],[8,11],[8,boty]])
        r.map[4][8] = r.map[5][8] = r.map[4][9] = r.map[5][9] = TILE_UP
        self.rooms[1][3][floor] = r

        r = Room('You are on the Castle Wall.')
        r.makewalls([[1,0],[1,boty],[rgtx,boty]])
        r.makewalls([[8,0],[8,13],[rgtx,13]])
        self.objects['RUBYS'] = Object(3, boty-2, r, CHAR['ruby'], 'RUBYS', 'They look expensive!')
        self.rooms[1][2][floor] = r

        r = Room('You are on the Castle Wall.')
        r.makewalls([[rgtx-1,0],[rgtx-1,boty],[0,boty]])
        r.makewalls([[15,0],[15,13],[0,13]])
        self.rooms[2][2][floor] = r

        r = Room('You are at the Bottom of the East Tower.')
        r.makewalls([[22,0],[22,6],[23,6],[23,11],[22,11],[22,boty]])
        r.makewalls([[15,0],[15,6],[14,6],[14,11],[15,11],[15,boty]])
        r.map[19][8] = r.map[18][8] = r.map[19][9] = r.map[18][9] = TILE_UP
        self.rooms[2][3][floor] = r

        # TOWERS
        floor = 7
        r = Room('You are in The East Tower. There are Stairs Going both up & down.')
        r.makewalls([[23,6],[23,11],[22,11],[22,12],[15,12],[15,11],[14,11],[14,6],[15,6],[15,5],[22,5],[22,6]])
        r.map[19][8] = r.map[19][9] = TILE_DN
        r.map[18][8] = r.map[18][9] = TILE_UP
        self.rooms[2][3][floor] = r

        r = Room('You are in The West Tower. There is a Passage to the East.')
        r.makewalls([[23,7],[9,7],[9,6],[8,6],[8,5],[1,5],[1,6],[0,6],[0,11],[1,11],[1,12],[8,12],[8,11],[9,11],[9,10],[23,10]])
        r.map[4][8] = r.map[4][9] = TILE_DN
        r.map[5][8] = r.map[5][9] = TILE_UP
        r.exits['E'] = 0,0,floor  # No room between towers
        self.rooms[1][3][floor] = r

        r = Room('You are in the Sorcerers Quarters. Two Beds and a large mirror are all that is left.', 'sorcerer')
        r.makewalls([[0,7],[5,7],[5,3],[19,3],[19,14],[5,14],[5,10],[0,10]])
        r.makewalls([[7,12],[10,12],[10,11],[7,11]], char=CHAR['solid'])
        r.makewalls([[7,5],[10,5],[10,6],[7,6]], char=CHAR['solid'])
        r.map[18][6] = Tile(CHAR['boxUL'])
        r.map[18][12] = Tile(CHAR['boxLL'])
        r.makewalls([[18,7],[18,11]], char=CHAR['vline'])
        r.exits['W'] = 1,3,floor
        r.actions[('LOOK','MIRROR')] = 'It looks Magical!'
        r.actions[('LOOK','BED')] = "It's Old."
        self.rooms[0][0][floor] = r

        r = Room('You are in the Sorcerers Quarters. Two Beds and a large mirror are all that is left.', 'sorcereropen')
        r.makewalls([[0,7],[5,7],[5,3],[19,3],[19,7],[23,7]])
        r.makewalls([[0,10],[5,10],[5,14],[19,14],[19,10],[23,10]])
        r.makewalls([[7,12],[10,12],[10,11],[7,11]], char=CHAR['solid'])
        r.makewalls([[7,5],[10,5],[10,6],[7,6]], char=CHAR['solid'])
        r.exits['W'] = 1,3,floor
        self.rooms[1][0][floor] = r

        r = Room('You are in the Sorcerers secret laboratory.', 'TEST')
        r.makewalls([[0,7],[5,7],[5,3],[23,3],[23,14],[5,14],[5,10],[0,10]])
        warptile = Tile(' '*widthmultiple)
        warptile.collide = self.sorcback
        r.map[0][8] = r.map[0][9] = warptile
        self.objects['BALL'] = Object(12, 12, r, CHAR['ball'], 'BALL', 'In the Crystal Ball....\nYou see a man in a winding passage,waving a Wand.', adjective='CRYSTAL')
        self.rooms[2][0][floor] = r

        # Top of towers
        floor = 8
        r = Room('You are at the top of the west tower.')
        r.map[0][8] = r.map[0][9] = r.map[9][8] = r.map[9][9] = Tile(CHAR['vline'])
        r.makewalls([[3,5],[6,5]], char=CHAR['hline'])
        r.makewalls([[3,12],[6,12]], char=CHAR['hline'])
        r.map[0][7] = r.map[0][6] = r.map[1][6] = r.map[1][5] = r.map[2][5] = TILE_WALL
        r.map[0][10] = r.map[0][11] = r.map[1][11] = r.map[1][12] = r.map[2][12] = TILE_WALL
        r.map[9][7] = r.map[9][6] = r.map[8][6] = r.map[8][5] = r.map[7][5] = TILE_WALL
        r.map[9][10] = r.map[9][11] = r.map[8][11] = r.map[8][12] = r.map[7][12] = TILE_WALL
        r.map[4][8] = r.map[4][9] = r.map[5][8] = r.map[5][9] = TILE_DN
        self.rooms[1][3][floor] = r

        floor = 8
        r = Room('You are at the top of the East Tower.')
        r.map[23][8] = r.map[23][9] = r.map[14][8] = r.map[14][9] = Tile(CHAR['vline'])
        r.makewalls([[17,5],[20,5]], char=CHAR['hline'])
        r.makewalls([[17,12],[20,12]], char=CHAR['hline'])
        r.map[23][7] = r.map[23][6] = r.map[22][6] = r.map[22][5] = r.map[21][5] = TILE_WALL
        r.map[23][10] = r.map[23][11] = r.map[22][11] = r.map[22][12] = r.map[21][12] = TILE_WALL
        r.map[14][7] = r.map[14][6] = r.map[15][6] = r.map[15][5] = r.map[16][5] = TILE_WALL
        r.map[14][10] = r.map[14][11] = r.map[15][11] = r.map[15][12] = r.map[16][12] = TILE_WALL
        r.map[18][8] = r.map[18][9] = r.map[19][8] = r.map[19][9] = TILE_DN
        self.objects['HARP'] = Object(15,7,r, CHAR['harp'],'HARP',"It's made of Gold!")
        self.objects['HARP'].actions['PLAY'] = self.playharp
        self.rooms[2][3][floor] = r

        # DUNGEON
        floor = 0
        r = Room('You are in the Dungeon entrace hall. There is a long staircase leading up.')  # sic
        r.makewalls([[0,7],[13,7],[13,6],[23,6],[23,11],[13,11],[13,10],[0,10]])
        r.map[22][8] = r.map[22][9] = TILE_UP
        r.map[13][8] = r.map[13][9] = Tile(CHAR['door'], collide='The Door is locked.')
        r.actions[('LOOK','DOOR')] = 'It looks very Strong.'
        r.actions[('OPEN','DOOR')] = self.opendoor
        r.actions[('UNLOCK','DOOR')] = self.opendoor
        r.exits[CHAR['up']] = 2,3,5
        self.rooms[4][1][floor] = r

        r = Room('You are in the Royal Wine Cellar. It is filled with many Barrels. At one end is a Large metal Door.')
        r.makewalls([[7,17],[7,14],[0,14],[0,0],[18,0],[18,14],[10,14],[10,17]])
        r.map[8][14] = r.map[9][14] = Tile(CHAR['door'], collide='The Door is locked.')
        r.map[2][6] = TILE_UP
        r.makewalls([[3,2],[6,2],[6,3],[3,3]], CHAR['solid'])
        r.makewalls([[3,8],[6,8],[6,9],[3,9]], CHAR['solid'])
        r.makewalls([[3,11],[6,11],[6,12],[3,12]], CHAR['solid'])
        r.makewalls([[12,2],[15,2],[15,3],[12,3]], CHAR['solid'])
        r.makewalls([[12,5],[15,5],[15,6],[12,6]], CHAR['solid'])
        r.makewalls([[12,8],[15,8],[15,9],[12,9]], CHAR['solid'])
        r.makewalls([[12,11],[15,11],[15,12],[12,12]], CHAR['solid'])
        r.actions[('LOOK', 'BARREL')] = "They're made of wood"
        r.actions[('FILL', 'FLASK')] = 'The Barrels are empty.'
        r.actions[('GET', 'BARREL')] = 'BARRELS are too heavy!'
        r.actions[('LOOK','DOOR')] = 'It looks very Strong.'
        r.actions[('OPEN','DOOR')] = self.opendoor2
        r.actions[('UNLOCK','DOOR')] = self.opendoor2
        self.objects['BAT'] = Monster(13, 7, r, CHAR['bat'], 'BAT', '')
        r.exits[CHAR['up']] = 0,1,1  # Go back to storage
        self.rooms[0][5][floor] = r

        r = Room('You are in a long Passageway.')
        r.makewalls([[7,0],[7,17]])
        r.makewalls([[10,0],[10,17]])
        self.rooms[0][4][floor] = r

        r = Room('You are in a Maze.')
        r.makewalls([[7,0],[7,2],[0,2],[0,17]])
        r.makewalls([[1,5],[10,5],[10,8],[3,8]])
        r.makewalls([[6,6],[6,7]])
        r.makewalls([[7,6],[9,6],[9,7],[7,7]], CHAR['wall2'])
        r.makewalls([[10,0],[10,2],[23,2],[23,8],[19,8]])
        r.makewalls([[23,11],[16,11],[16,15],[16,5],[20,5]])
        r.makewalls([[13,5],[13,15],[11,15],[11,11],[9,11],[12,11]])
        r.makewalls([[12,12],[12,14]], CHAR['wall2'])
        r.makewalls([[2,17],[2,11],[7,11]])
        r.makewalls([[3,15],[7,15]])
        r.makewalls([[4,17],[23,17]])
        r.makewalls([[4,13],[9,13],[9,16]])
        r.makewalls([[18,16],[18,14]])
        r.makewalls([[23,15],[21,15],[21,13],[23,13]])
        r.makewalls([[22,14],[23,14]], CHAR['wall2'])
        self.objects['SPIDER'] = Monster(16, 3, r, CHAR['sspider'], 'SPIDER', '', adjective='SMALL')
        self.rooms[0][3][floor] = r

        r = Room('You are in a Maze.')
        r.makewalls([[0,8],[0,0],[23,0],[23,17],[17,17],[17,8],[13,8],[13,17],[0,17]])
        r.makewalls([[13,7],[13,6],[6,6],[6,12]])
        r.makewalls([[0,15],[0,13]])
        r.makewalls([[0,11],[3,11],[3,3],[20,3],[20,14]])
        r.makewalls([[3,12],[3,14],[10,14],[10,8],[8,8],[8,13]])
        r.makewalls([[9,9],[9,13]], CHAR['wall2'])
        r.makewalls([[16,17],[16,9],[15,9],[15,17],[14,17],[14,9]], CHAR['wall2'])
        self.rooms[1][3][floor] = r

        r = Room('You are in a Maze.')
        r.makewalls([[0,0],[0,17]])
        r.makewalls([[1,5],[3,5]])
        r.makewalls([[1,9],[2,9],[2,13],[1,13]])
        r.makewalls([[1,10],[1,12]], CHAR['wall2'])
        r.makewalls([[2,0],[2,3],[21,3],[21,8]])
        r.makewalls([[5,4],[5,15],[3,15],[3,17],[8,17],[8,7],[2,7]])
        r.makewalls([[4,16],[6,16],[6,8],[7,8],[7,16]], CHAR['wall2'])
        r.makewalls([[4,0],[23,0],[23,11],[18,11],[18,14],[20,14],[20,12]])
        r.makewalls([[19,12],[19,13]], CHAR['wall2'])
        r.makewalls([[23,14],[23,17],[11,17],[11,6],[18,6],[18,8],[12,8]])
        r.makewalls([[12,7],[17,7]], CHAR['wall2'])
        self.rooms[0][2][floor] = r

        r = Room('You are in a Maze.')  # kb, goldbar
        r.makewalls([[0,0],[0,17],[23,17],[23,14]])
        r.makewalls([[3,15],[3,0],[8,0],[8,6],[8,3],[21,3],[21,8]])
        r.makewalls([[23,0],[11,0]])
        r.makewalls([[6,2],[6,16],[10,16]])
        r.makewalls([[6,8],[18,8],[18,6],[11,6],[11,12],[6,12]])
        r.makewalls([[17,7],[12,7]], CHAR['wall2'])
        r.makewalls([[7,9],[10,9],[10,11],[7,11],[7,9]], CHAR['wall2'])
        r.map[8][10] = Tile('k')
        r.map[9][10] = Tile('b')
        r.makewalls([[23,3],[23,11],[13,11],[13,14],[8,14]])
        r.map[13][15] = TILE_WALL
        r.makewalls([[18,11],[18,14],[20,14],[20,11]])
        r.makewalls([[19,12],[19,13]], CHAR['wall2'])
        r.makewalls([[15,16],[15,13]])
        self.objects['GOLDBAR'] = Object(22, 13, r, CHAR['gold'], 'GOLDBAR', 'It looks Expensive!')
        self.rooms[0][1][floor] = r

        r = Room('You are in a Maze.')
        r.makewalls([[0,3],[0,11]])
        r.makewalls([[0,0],[5,0],[3,0],[3,14],[0,14],[0,17],[5,17],[5,6],[4,6]])
        r.makewalls([[3,15],[1,15],[1,16],[4,16],[4,7]], CHAR['wall2'])
        r.makewalls([[6,17],[10,17],[10,12],[11,12],[11,8],[20,8],[20,15],[20,2],[13,2],[13,0],[11,0]])
        r.makewalls([[8,0],[8,11],[7,10],[7,15]])
        r.makewalls([[9,2],[10,2],[10,4],[17,4],[17,6],[9,6]])
        r.makewalls([[9,3],[9,5],[16,5]], CHAR['wall2'])
        r.makewalls([[7,3],[6,3]])
        r.makewalls([[16,0],[23,0],[23,17],[13,17],[13,14],[14,14],[14,11],[17,11],[17,16]])
        r.makewalls([[14,15],[14,16],[15,16],[15,12],[16,12],[16,16]], CHAR['wall3'])
        self.rooms[1][1][floor] = r

        r = Room('You are in a Maze.')
        r.makewalls([[0,11],[0,0],[6,0],[6,1],[7,1],[7,0],[14,0],[14,1],[15,1],[15,0],[22,0],[22,1],[23,1],[23,0],[23,17],[16,17]])
        r.makewalls([[1,5],[3,5]])
        r.makewalls([[3,2],[3,3],[5,3],[5,17],[0,17],[0,14],[3,14],[3,7],[4,7]])
        r.makewalls([[6,5],[8,5]])
        r.makewalls([[4,8],[4,16],[1,16],[1,15],[3,15]], CHAR['wall2'])
        r.makewalls([[8,17],[8,7]])
        r.makewalls([[13,17],[11,17],[11,6],[18,6],[18,8],[12,8]])
        r.makewalls([[12,7],[17,7]], CHAR['wall2'])
        r.makewalls([[8,3],[10,3],[10,2],[11,2],[11,3],[18,3],[18,2],[19,2],[19,3],[21,3],[21,8]])
        self.objects['SPIDER2'] = Monster(18, 13, r, CHAR['bspider'], 'SPIDER', '', adjective='BIG')
        self.rooms[1][2][floor] = r

        r = Room('You are in a Winding Passage.')
        r.makewalls([[10,0],[10,3],[6,3],[6,15],[19,15],[19,4],[23,4]])
        r.makewalls([[13,0],[13,6],[9,6],[9,12],[16,12],[16,1],[23,1]])
        r.map[14][13] = r.map[14][14] = Trap(walls=[[10,13],[10,15],[16,15],[16,13]], fill=[[11,13],[15,13],[15,14],[11,14]])
        self.rooms[1][0][floor] = r

        r = Room('You ware in a Winding Passage', 'winding')
        r.makewalls([[0,1],[6,1],[6,14],[19,14],[19,11]])
        r.makewalls([[0,4],[3,4],[3,17],[22,17],[22,8],[16,8],[16,11],[9,11],[9,6],[16,6],[16,7],[16,6],[13,6],[13,0]])
        r.makewalls([[7,3],[10,3],[10,0]])
        r.makewalls([[10,7],[15,7]], CHAR['wall2'])
        r.makewalls([[10,8],[15,8]], CHAR['wall3'])
        r.makewalls([[10,9],[15,9]], CHAR['wall2'])
        r.makewalls([[10,10],[15,19]], CHAR['wall3'])
        r.exits['S'] = 2,2,floor
        self.rooms[2][0][floor] = r

        r = Room("You are in the King's Secret room. The walls are many colors.")
        r.makewalls([[10,0],[10,2]])
        r.makewalls([[13,0],[13,2]])
        r.makewalls([[10,3],[4,3],[4,13],[19,13],[19,3],[13,3]], CHAR['wall2'])
        r.map[10][3] = r.map[8][3] = r.map[6][3] = r.map[4][3] = TILE_SOLID
        r.map[4][5] = r.map[4][7] = r.map[4][9] = r.map[4][11] = r.map[4][13] = TILE_SOLID
        r.map[6][13] = r.map[8][13] = r.map[10][13] = r.map[13][13] = r.map[15][13] = r.map[17][13] = TILE_SOLID
        r.map[19][13] = r.map[19][11] = r.map[19][9] = r.map[19][7] = r.map[19][5] = r.map[19][3] = TILE_SOLID
        r.map[17][3] = r.map[15][3] = r.map[13][3] = TILE_SOLID
        r.exits['N'] = 2,0,floor
        self.objects['SCEPTER'] = Object(11, 11, r, CHAR['scepter'], 'SCEPTER', 'It looks Expensive!')
        self.objects['SCEPTER'].actions['WAVE'] = self.wavescepter
        self.rooms[2][2][floor] = r

        r = Room('You are in a Winding passage.')
        r.makewalls([[10,17],[10,13],[5,13],[5,11],[0,11],[0,3],[11,3],[11,7],[23,7]])
        r.makewalls([[13,17],[13,10],[8,10],[8,6],[3,6],[3,8],[8,8],[8,10],[23,10]])
        r.makewalls([[4,7],[7,7]], CHAR['wall2'])

        trap = Trap(walls=[[11,8],[11,9]], fill=[[12,8],[23,8],[23,9],[12,9]])
        r.map[16][9] = r.map[16][8] = trap
        self.rooms[2][1][floor] = r

        r = Room('You are in the Dungeon Annex.')
        r.makewalls([[0,7],[5,7],[5,4],[9,4],[9,0]])
        r.makewalls([[14,0],[14,4],[21,4],[21,7],[23,7]])
        r.makewalls([[0,10],[5,10],[5,14],[10,14],[10,17]])
        r.makewalls([[13,17],[13,14],[21,14],[21,10],[23,10]])
        self.rooms[3][1][floor] = r

        r = Room('You are in the Toture room. There are chains on one wall, and a Large Table.')  # sic
        r.makewalls([[10,0],[10,2],[2,2],[2,15],[21,15],[21,2],[13,2],[13,0]])
        r.makewalls([[5,5],[5,7],[6,7],[6,5]], CHAR['solid'])
        r.map[20][7] = r.map[20][10] = Tile(CHAR['chain'])
        self.rooms[3][0][floor] = r

        r = Room('You are in the Dungeon')
        r.makewalls([[9,17],[9,14],[0,14],[0,0],[23,0],[23,14],[14,14],[14,17]])
        r.makewalls([[6,13],[6,4],[1,4]])
        r.makewalls([[1,8],[5,8]])
        r.makewalls([[8,1],[8,3],[16,3],[16,1]])
        r.map[6][6] = r.map[6][12] = r.map[9][3] = None
        r.makewalls([[17,14],[17,4],[22,4]])
        r.makewalls([[22,8],[18,8]])
        r.map[17][12] = r.map[17][6] = None
        self.rooms[3][2][floor] = r

        # Items that aren't visible
        self.objects['FLASK'] = Object(0, 0, None, CHAR['flask'], 'FLASK', "It's empty", adjective='WINE', score=False)
        self.objects['FLASK'].full = False
        self.objects['GEM'] = Object(0, 0, None, CHAR['gem'], 'GEM', "It looks Expensive!", adjective="LARGE")
        self.objects['NECKLACE'] = Object(0, 0, None, CHAR['necklace'], 'NECKLACE', 'On the back it says: Protection from Traps.')
        self.objects['NECKLACE'].actions['WEAR'] = lambda: "Ok, I'm wearing it."
        self.objects['KEY'] = Object(0, 0, None, CHAR['key'], 'KEY', 'It looks Old!', score=False)
        self.objects['BOOK'] = Object(0, 0, None, CHAR['book'], 'BOOK', "It is titled 'The Gate'", score=False)
        self.objects['BOOK'].actions['READ'] = self.readbook
        self.objects['GLASSES'] = Object(0, 0, None, CHAR['glasses'], 'GLASSES', "They're Bifocals", score=False)


class Window(object):
    ''' Window for drawing things relative to the screen.
        Simulates a curses window, without requiring curses library.
    '''
    def __init__(self, h, w, y, x):
        self.h = h
        self.w = w
        self.y = y
        self.x = x

    def clear(self):
        ''' clear the window '''
        for y in range(self.y, self.y+self.h):
            printxy(y, self.x, ' '*self.w)

    def addstr(self, y, x, s, clearline=False):
        ''' Add string relative to window '''
        for row, line in enumerate(s.splitlines()):
            if clearline:
                printxy(self.y+y+row, self.x+x, line + ' '*(self.w-len(s)))
            else:
                printxy(self.y+y+row, self.x+x, line)

    def box(self):
        ''' Draw box around window '''
        printxy(self.y, self.x, '┌' + '─'*(self.w-2) + '┐')
        for i in range(self.y+1, self.y+self.h-1):
            printxy(i, self.x, '│')
            printxy(i, self.x+self.w-1, '│')
        printxy(self.y+self.h-1, self.x, '└' + '─'*(self.w-2) + '┘')


# Screen/window coordinates
# --------------------------
roomw, roomh = 24*widthmultiple, 18
lftx = 0  # Top/left position of room
topy = 0
rgtx = roomw//widthmultiple-1
boty = roomh-1

msgx = roomw + 4
msgy = roomh // 2 + 1
roomdescx = 1
roomdescy = roomh + 4
cmdx = roomw + 4
cmdy = roomdescy + 1

# Define TILES that can be reused.
# If they have a custom actions, need new object instance
TILE_WALL = Tile(CHAR['wall'])
TILE_DN = Tile(CHAR['dn'])
TILE_UP = Tile(CHAR['up'])
TILE_SOLID = Tile(CHAR['solid'])


def main():
    ''' Main program loop '''
    width, height = shutil.get_terminal_size()
    hide_cursor()
    clear_screen()

    g = Game()
    boxdesc = Window(6, roomw+3, roomdescy, roomdescx)
    boxroomborder = Window(roomh+2, roomw+2, 1, 1)
    boxroom = Window(roomh, roomw, 2, 2)
    boxmsg = Window(8, 17, msgy, msgx)
    boxitems = Window(8, 30, 1, msgx)
    boxcmd = Window(1, 15, cmdy, cmdx)

    g.set_windows(boxroom, boxdesc, boxitems, boxmsg, boxcmd, boxroomborder)
    g.drawroom()
    g.drawplayer()
    g.drawitems()
    g.drawcmd()

    cmdbuf = ''  # Keyboard command buffer
    while True:
        ch = getch(timeout=0.2)
        if ch is not None:
            if ch in ['up', 'down', 'left', 'right']:
                dir = {'up': 'N', 'down': 'S',
                       'left': 'W', 'right': 'E'}.get(ch)
                ok = g.move(dir)
                if not ok:  # press a key to continue...
                    ch = getch()
                    break

            elif ch == 'enter':  # Command entered
                cmd = g.cmd(cmdbuf)
                if cmd == 'QUIT':
                    break
                cmdbuf = ''

            elif ch == 'backspace':
                if cmdbuf != '':
                    cmdbuf = cmdbuf[:-1]
                    g.drawcmd(cmdbuf)

            else:   # Character entered, add to command string
                cmdbuf = cmdbuf + ch.upper()
                g.drawcmd(cmdbuf)

        elif g.monster and g.monster.health > 0:
            # Move monster toward player
            msg = g.movemonster()
            if msg == 'DEAD':
                break

    # GAME OVER when break out of previous loop
    while True:
        clear_screen()
        if g.currentroom is None:
            printxy(1, 4, "You've escaped the Castle!")
        else:
            printxy(1, 4, "You're have failed to Escape!")  # sic
        printxy(4, 2, 'You have collected these Treasures...')
        printxy(9, 2, 'You have killed these Monsters.......')
        treas = []
        monst = []
        score = 0
        for ob in g.objects.values():
            if isinstance(ob, Monster) and ob.health <= 0:
                monst.append(ob.char)
                score += 100
            elif ob.score and ob.loc in ['inv', g.getroom('entrance')]:
                treas.append(ob.char)
                score += 50
        if len(treas) == 0:
            printxy(11, 6, 'NONE')
        else:
            printxy(6, 4, ' '.join(treas))
        if len(treas) == 0:
            printxy(11, 6, 'NONE')
        else:
            printxy(11, 6, ' '.join(monst))

        printxy(17, 8, 'Your score: {}'.format(score))
        printxy(18, 8, '(1550 is perfect)')

        ch = getch()
        if ch not in ['up', 'down', 'left', 'right']:
            break


if __name__ == '__main__':
    main()
