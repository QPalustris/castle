Castle Adventure

Python Remake of old [DOS game](https://en.wikipedia.org/wiki/Castle_Adventure)

Cross-platform, no dependencies. Python 3.6+. Requires terminal that supports ANSI
escape codes, which includes the terminal in Windows 10 and most Linux- OS's. 
This grew from an experiment in cross-platform terminal programming without using
the curses library.


Classic mode:

    python castle.py

Also supports an emoji mode where the objects and characters are emojis!
Requires a supported font in the terminal.

    python castle.py -e


For emoji mode, a suitable font may need to be installed, for example:

    sudo apt install fonts-emojione

Then configure the terminal to use this font. Windows users may have more work to do.


![Screenshot of Emoji Mode](screenshot.png)


Known differences from the original:
    - No save/load functionality
    - No high scores
    - No sound
    - No intro screen or instructions

Tried to maintain the spelling mistakes throughout, that's
half the fun. Not having the original source, there's probably
commands and actions that are not implemented.
